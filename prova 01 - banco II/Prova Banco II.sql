drop table if exists proposta_matricula cascade;
drop table if exists aluno cascade;
drop table if exists disciplina cascade;
drop table if exists professor cascade;


create table proposta_matricula (
	matricula_aluno 		integer	not null,
	cod_disciplina		char(6)	not null);
create table aluno (
	matricula		serial			not null,
	nome			varchar(40)		not null,
	telefone		varchar(11)		);
create table disciplina (
	cod			char(6)		not null,
	nome			varchar(100)	not null,
	carga_horaria		smallint	not null,
	matricula_professor	integer	not null);
create table professor (
	matricula		serial			not null,
	nome			varchar(40)		not null,
	data_admissao	date			not null,
	email			varchar(250)		);


ALTER TABLE disciplina
ADD CONSTRAINT pk_cod PRIMARY KEY (cod);

ALTER TABLE aluno
ADD CONSTRAINT pk_matricula PRIMARY KEY (matricula);

ALTER TABLE professor
ADD CONSTRAINT pk_matricula_prof PRIMARY KEY (matricula);


ALTER TABLE disciplina
ADD CONSTRAINT matricula_professor_unico UNIQUE (matricula_professor);

ALTER TABLE proposta_matricula
ADD CONSTRAINT fk_matricula_aluno
FOREIGN KEY (matricula_aluno) REFERENCES aluno(matricula);


ALTER TABLE proposta_matricula
ADD CONSTRAINT fk_cod_disciplina
FOREIGN KEY (cod_disciplina) REFERENCES disciplina(cod);


ALTER TABLE disciplina
ADD CONSTRAINT fk_matricula_professor
FOREIGN KEY (matricula_professor) REFERENCES professor(matricula);


ALTER TABLE disciplina 
ADD CONSTRAINT carga_horaria_check 
CHECK (
	carga_horaria = 30
	OR carga_horaria = 60
	OR carga_horaria = 90
);

CREATE OR REPLACE FUNCTION TRG_VERIFICAR_MATERIA() RETURNS TRIGGER AS $$
DECLARE
	prof_nome varchar;
BEGIN
	IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
		IF ((SELECT count(matricula_professor) FROM disciplina WHERE new.matricula_professor = matricula_professor) >= 5) THEN
			SELECT	nome
			INTO	prof_nome
			FROM	professor
			WHERE	matricula = new.matricula_professor;
			RAISE EXCEPTION 'O professor % não pode ministrar mais que 5 disciplinas! ', l_professor_nome using errcode='23514';
  		END IF;
	RETURN NEW;
	END IF;
  RETURN NEW;
END;
$$ LANGUAGE PLPGSQL;


CREATE TRIGGER TRG_VERIFICAR_MATERIA
BEFORE
INSERT
OR
UPDATE OF MATRICULA_PROFESSOR ON DISCIPLINA
FOR EACH ROW EXECUTE PROCEDURE TRG_VERIFICAR_MATERIA();



CREATE VIEW VIEW_PROFESSOR_DISCIPLINAS AS
SELECT PROF.NOME AS NOME_PROFESSOR,
	PROF.EMAIL AS EMAIL_PROFESSOR,
	COALESCE(DISC.NOME,'(nenhuma disciplina associada)') AS NOME_DISCIPLINA
FROM PROFESSOR PROF
LEFT OUTER JOIN DISCIPLINA AS DISC ON (PROF.MATRICULA = DISC.MATRICULA_PROFESSOR)
ORDER BY PROF.NOME ASC;



SELECT AL.NOME AS NOME_ALUNO,
	AL.MATRICULA AS MATRICULA_ALUNO,
	SUM(DISC.CARGA_HORARIA) AS CARGA_HORARIA_TOTAL
FROM ALUNO AS AL
INNER JOIN PROPOSTA_MATRICULA PROP ON (AL.MATRICULA = PROP.MATRICULA_ALUNO)
INNER JOIN DISCIPLINA AS DISC ON (PROP.COD_DISCIPLINA = DISC.COD)
GROUP BY AL.NOME,
	AL.MATRICULA
HAVING SUM(DISC.CARGA_HORARIA) < 150
ORDER BY SUM(DISC.CARGA_HORARIA) ASC;



SELECT PROF.NOME AS NOME_PROFESSOR,
	PROF.DATA_ADMISSAO AS DATA_ADMISSAO_PROFESSOR
FROM PROFESSOR AS PROF
INNER JOIN DISCIPLINA AS DISC ON (PROF.MATRICULA = DISC.MATRICULA_PROFESSOR)
LEFT JOIN PROPOSTA_MATRICULA AS PROP ON (DISC.COD = PROP.COD_DISCIPLINA)
WHERE NOT EXISTS
								(SELECT *
									FROM PROPOSTA_MATRICULA
									WHERE PROP.COD_DISCIPLINA = COD_DISCIPLINA)
ORDER BY PROF.NOME ASC;

insert into professor (nome, data_admissao, email) values
	('Fernando Santos', '2018-05-20', 'fernandosantos@gmail.com'),
	('Mario Santos', '2016-03-30', 'mariosantos@gmail.com'),
	('Pedro Pereira', '1996-01-11', 'pedropereira@gmail.com'),
	('Fernando Borges', '2010-01-01', 'fernandoborges@gmail.com'),
	('Mario Jorge', '2010-11-07', 'mariojorge@gmail.com');


insert into aluno (nome, telefone) values
	('Eli Marins', '7834-5612'),
	('Cristiano Filho', '5623-1234'),
	('Daison Santos', '6587-4139'),
	('Enzo Santana', '1381-1379'),
	('Caio Junior', '1123-1329');

	
insert into disciplina (cod, nome, carga_horaria, matricula_professor) values
	('ECAIEC', 'Introdução a engenharia civil', 60, (select matricula from professor where nome = 'Fernando Santos')),
	('ECAPEE', 'Probabilidade', 30, (select matricula from professor where nome = 'Fernando Santos')),
	('ECAMAT', 'Matematica 1', 30, (select matricula from professor where nome = 'Fernando Santos')),									
	('ECARL1', 'Raciocinio Logico', 90, (select matricula from professor where nome = 'Fernando Santos')),
	('ECACA2', 'Calculo 2', 90, (select matricula from professor where nome = 'Pedro Pereira')),
	('ECALI1', 'Linguagem interpretação', 60, (select matricula from professor where nome = 'Fernando Santos')),
	('ECACA1', 'Calculo 1', 90, (select matricula from professor where nome = 'Mario Jorge'));
	

										  
insert into proposta_matricula (matricula_aluno, cod_disciplina) values
	((select matricula from aluno where nome = 'Eli Marins'), (select cod from disciplina where nome = 'Probabilidade')),
	((select matricula from aluno where nome = 'Eli Marins'), (select cod from disciplina where nome = 'Probabilidade')),
	((select matricula from aluno where nome = 'Cristiano Filho'), (select cod from disciplina where nome = 'Matematica 1')),
	((select matricula from aluno where nome = 'Cristiano Filho'), (select cod from disciplina where nome = 'Banco de Dados 2')),
	((select matricula from aluno where nome = 'Daison Santos'), (select cod from disciplina where nome = 'Calculo 2')),
	((select matricula from aluno where nome = 'Enzo Santana'), (select cod from disciplina where nome = 'Linguagem interpretação')),
	((select matricula from aluno where nome = 'Caio Junior'), (select cod from disciplina where nome = 'Calculo 1'));









