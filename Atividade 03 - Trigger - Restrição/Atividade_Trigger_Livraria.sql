drop table if exists leitor cascade;
drop table if exists livro cascade;
drop table if exists emprestimo cascade;


	create table leitor (
	matricula serial not null,
	nome varchar(40) not null,
	telefone varchar(11) not null,
	constraint pk_leitor
	primary key (matricula));

	create table livro (
	isbn char(13) not null,
	titulo varchar(200) not null,
	tipo char(1) not null,
	constraint pk_livro
	primary key (isbn),
	constraint ch_livro_tipo
	check (tipo in ('E','C')));

		create table emprestimo (
		id serial not null,
		isbn_livro char(13) not null,
		data_emprestimo date not null,
		data_devolucao 	date 	null,
		matricula_leitor integer not null,
		constraint pk_emprestimo
		primary key (id),
		constraint ch_emprestimo_datas
		check (data_devolucao > data_emprestimo));

	alter table emprestimo
	add constraint fk_emprestimo_livro
	foreign key (isbn_livro)
	references livro,
	add constraint fk_emprestimo_leitor
	foreign key (matricula_leitor)
	references leitor;
	
create or replace function upd_emprestimo()
returns trigger 
as
$$
declare 
		livro_nome varchar; 
		livro_tipo char; 
	begin 
	
	select titulo, tipo
	into  livro_nome, livro_tipo
	from livro
	where livro.isbn = new.isbn_livro; 
	
	if(livro_tipo = 'E') then
	raise notice 'Emprestimo efetuado com sucesso!'; 
	return new; 
	
	
	else
	raise exception  'Você não pode fazer o emprestimo do livro selecionado definido apenas consulta-lo!';

		END IF; 
		END; 

	$$LANGUAGE plpgsql; 
	
	CREATE TRIGGER upd_emprestimo
BEFORE UPDATE or INSERT
ON emprestimo
FOR EACH ROW
EXECUTE PROCEDURE upd_emprestimo();

--- scripts para criação de dados para testes

insert into leitor (nome, telefone) values
('Claudio Neiva' ,'71988776655'),
('Maria da Silva' ,'71977665544'),
('Pedro Santos' ,'75989067856');
insert into livro (isbn, titulo, tipo) values
('9788533302273','Plantando Caju','C'),
('7575755655555','Colhendo Caju','C'),
('7272262252242','Vendendo Caju','E'),
('7177167178763','Receitas com Caju','E'),
('7576567365356','Segredos do Caju','C');



--Para testes, considere que o seguinte script deve ter sucesso, pois contém apenas livros do
--tipo ‘E’ (livros para empréstimo):
insert into emprestimo (isbn_livro, data_emprestimo, data_devolucao, matricula_leitor) values
	('7272262252242', '2020-09-01', '2020-09-08', (select matricula from leitor where nome = 'Claudio Neiva')),
	('7177167178763', '2020-09-01', '2020-09-10', (select matricula from leitor where nome = 'Maria da Silva')),
	('7272262252242', '2020-09-10',null, (select matricula from leitor where nome = 'Maria da Silva')),
	('7177167178763', '2020-09-15', '2020-09-17', (select matricula from leitor where nome = 'Pedro Santos'));

select  * from emprestimo; 




--Para testes, considere que o seguinte script deve FALHAR, pois contém um livro do tipo ‘C’
--(livro para consulta):

insert into emprestimo (isbn_livro, data_emprestimo, data_devolucao, matricula_leitor)
values
('7272262252242','2020-10-05','2020-10-09',(select matricula from leitor where nome =
'Pedro Santos')),
('7576567365356','2020-10-06',null,(select matricula from leitor where nome = 'Claudio
Neiva'));

 